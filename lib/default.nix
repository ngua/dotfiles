final: _:

{
  kmonads = devices:
    let
      withDefCfg = { input, cfg, name }:
        final.attrsets.nameValuePair
          name
          {
            device = "/dev/input/by-path/${input}";
            config = builtins.readFile cfg;
            defcfg = {
              enable = true;
              fallthrough = true;
              allowCommands = true;
            };
          };
    in
    builtins.listToAttrs (builtins.map withDefCfg devices);
}
