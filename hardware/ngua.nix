{ config, lib, pkgs, ... }:

{
  boot = {
    initrd.availableKernelModules = [
      "xhci_pci"
      "ahci"
      "usb_storage"
      "usbhid"
      "sd_mod"
      "rtsx_pci_sdmmc"
    ];
    initrd.kernelModules = [];
    kernelModules = [ "kvm-intel" ];
    extraModulePackages = [];
  };

  fileSystems = {
    "/" =
      {
        device = "/dev/disk/by-uuid/43aee542-8ca8-4ac5-8709-6361fcb7f312";
        fsType = "xfs";
      };

    "/boot" =
      {
        device = "/dev/disk/by-uuid/DEEE-0A53";
        fsType = "vfat";
      };
  };

  swapDevices =
    [
      {
        device = "/dev/disk/by-uuid/5faf2021-29d6-4449-bd9c-d9f0434da504";
      }
    ];

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
}
