{ config, lib, pkgs, ... }:

{
  boot = {
    initrd.availableKernelModules = [
      "xhci_pci"
      "nvme"
      "thunderbolt"
      "usb_storage"
      "sd_mod"
      "rtsx_pci_sdmmc"
    ];
    initrd.kernelModules = [ ];
    kernelModules = [ "kvm-intel" ];
    extraModulePackages = [ ];
    extraModprobeConfig = ''
      options snd-intel-dspcfg dsp_driver=1
    '';
  };

  hardware.enableAllFirmware = true;

  services.thermald.enable = lib.mkDefault true;

  fileSystems = {
    "/" =
      {
        device = "rpool/root/nixos";
        fsType = "zfs";
      };

    "/home" =
      {
        device = "rpool/home";
        fsType = "zfs";
      };

    "/boot" =
      {
        device = "/dev/disk/by-label/boot";
        fsType = "vfat";
      };

    # To make Docker "work" with ZFS without using the `zfs` driver
    "/var/lib/docker" =
      {
        device = "/dev/zvol/rpool/docker";
        fsType = "ext4";
      };
  };

  swapDevices =
    [
      {
        device = "/dev/disk/by-label/swap";
      }
    ];

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
}
