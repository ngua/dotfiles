{ config, lib, pkgs, ... }:

{
  boot = {
    initrd.availableKernelModules = [
      "nvme"
      "xhci_pci"
      "usb_storage"
      "sd_mod"
      "rtsx_pci_sdmmc"
    ];
    initrd.kernelModules = [ ];
    kernelModules = [ "kvm-amd" ];
    extraModulePackages = [ ];
  };

  hardware.firmware = [ pkgs.rtw89-firmware ];

  fileSystems = {
    "/" =
      {
        device = "rpool/root/nixos";
        fsType = "zfs";
      };

    "/home" =
      {
        device = "rpool/home";
        fsType = "zfs";
      };

    "/boot" =
      {
        device = "/dev/disk/by-label/boot";
        fsType = "vfat";
      };
  };

  swapDevices =
    [
      {
        device = "/dev/disk/by-label/swap";
      }
    ];

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
}
