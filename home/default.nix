{ config, pkgs, lib, host, inputs, ... }:

{
  imports = [
    (./. + "/${host.name}.nix")
  ];

  home = {
    homeDirectory = "/home/${host.user}";
    keyboard = null;
    sessionVariables = {
      EDITOR = "emacs -nw";
      VISUAL = "emacs -nw";
    };
    pointerCursor = {
      x11.enable = true;
      package = pkgs.vanilla-dmz;
      name = "Vanilla-DMZ-AA";
    };
    sessionPath = [ ".local/bin" ];
    file = {
      ".haskeline".text = ''
        editMode: Vi
        maxHistorySize: Just 100000
        historyDuplicates: IgnoreConsecutive
      '';
      ".ghci".source = ../config/ghci;
      ".cabal/config".text = ''
        repository hackage.haskell.org
          url: http://hackage.haskell.org/

        remote-repo-cache: /home/${host.user}/.cabal/packages
        world-file: /home/${host.user}/.cabal/world
        extra-prog-path: /home/${host.user}/.cabal/bin
        documentation: False
        build-summary: /home/${host.user}/.cabal/logs/build.log
        remote-build-reporting: anonymous
        jobs: $ncpus
        installdir: /home/${host.user}/.cabal/bin

        program-default-options
          ghc-options: -haddock -j4 +RTS -A32m -RTS
      '';

      ".local/bin/random-bg" = {
        executable = true;
        text = ''
          #!/usr/bin/env bash
          wallpapers=(~/dotfiles/wallpapers/*)
          wallpaper=''${wallpapers[$RANDOM % ''${#wallpapers[@]} ]}
          feh --bg-fill $wallpaper
        '';
      };

      ".local/bin/calc" = {
        executable = true;
        text = ''
          #!/usr/bin/env bash
          rofi -show calc -modi calc -no-show-match -no-sort -disable-history
        '';
      };
    };

    packages = with pkgs; [ spotify-player ];
  };

  xdg = {
    enable = true;
    userDirs.enable = true;
    configFile = {
      "tridactyl/tridactylrc".source = ../config/tridactylrc;
      "floskell/config.json".source = ../config/floskell.json;
      "wezterm/wezterm.lua".source = ../config/wezterm.lua;
      "betterlockscreenrc".source = ../config/betterlockscreenrc;

      "mpv/mpv.conf".text = ''
        hwdec=auto-safe
        vo=gpu
        profile=gpu-hq
      '';

      "fcitx/conf/fcitx-classic-ui.config".text = ''
        [ClassicUI]
        SkinType=dark
      '';

      "fcitx/config".text = ''
        [Hotkey]
        TriggerKey=ALT_SPACE
        [Program]
        [Output]
        [Appearance]
      '';
    };
  };

  programs = {
    home-manager = { enable = true; };
    gpg.enable = true;
    eww = {
      enable = true;
      configDir = ../config/eww/bar;
    };
    doom-emacs = {
      enable = true;
      doomDir = ../config/doom;
      extraPackages = epkgs: [ epkgs.vterm epkgs.nerd-icons ];
    };
    bat = {
      enable = true;
      config = { theme = "Nord"; };
    };
    zathura = {
      enable = true;
      extraConfig = ''
        set zoom-step 20
        set statusbar-home-tilde true
        set recolor "true"
        set recolor-darkcolor \#e0e0e0
        set recolor-lightcolor \#46484a
        map <C--> zoom out
        map <C-+> zoom in
        map <Space> scroll down
        map R rotate rotate-ccw

        # match default [normal] bindings
        map [fullscreen] a adjust_window best-fit
        map [fullscreen] s adjust_window width
      '';
    };
    feh = {
      enable = true;
      keybindings = {
        "zoom_out" = "C-minus";
        "zoom_in" = [ "C-equal" "C-plus" ];
      };
    };
    skim = {
      enable = true;
      enableZshIntegration = true;
      defaultCommand = "fd -t f";
      defaultOptions = [ "--no-height" ];
    };
    firefox = {
      enable = true;
      package = pkgs.firefox.override {
        nativeMessagingHosts = [ pkgs.tridactyl-native ];
      };
      profiles.default = {
        extensions = with pkgs.nur.repos.rycee.firefox-addons; [
          tridactyl
          darkreader
          ublock-origin
          ublacklist
        ];
        settings = {
          "browser.autofocus" = false;
          "browser.startup.homepage" = "https://nixos.org";
          "browser.search.suggest.enabled" = false;
          "browser.fullscreen.autohide" = false;
          "browser.aboutConfig.showWarning" = false;
          "full-screen-api.ignore-widgets" = true;
          "general.smoothScroll" = false;
          "pdfjs.disabled" = true;
          "extensions.pocket.enabled" = false;
          "font.default.x-western" = "Noto Sans";
          "font.name.monospace.x-western" = "Fira Code";
          "font.name.sans-serif.x-western" = "Noto Sans";
          "font.name.serif.x-western" = "Noto Serif";
          "browser.display.use_document_fonts" = 0;
        };

      };
    };
    zsh.enable = true;
    git = {
      enable = true;
      userName = "Rory Tyler Hayford";
      userEmail = "rory.hayford@protonmail.com";
      extraConfig = {
        pull.rebase = false;
        push.default = "nothing";
      };
    };
    rofi = {
      enable = true;
      package = pkgs.rofi.override {
        plugins = [ pkgs.rofi-calc pkgs.rofi-pass ];
      };
      theme = ../config/rofi/theme.rasi;
      font = "Fira Code 12";
      location = "top-left";
      pass = {
        enable = true;
        stores = [ "$HOME/.password-store" ];
      };
      extraConfig = {
        modi = "run,window,windowcd,drun,calc,combi";
        width = 100;
        columns = 8;
        hide-scrollbar = true;
        lines = 2;
        line-margin = 0;
        line-padding = 1;
        bw = 0;
        color-normal = "#222222, #b1b4b3, #222222, #4c566a, #b1b4b3";
        color-urgent = "#222222, #b1b4b3, #222222, #77003d, #b1b4b3";
        color-active = "#222222, #b1b4b3, #222222, #007763, #b1b4b3";
        color-window = "#222222, #222222, #b1b4b3";
        kb-row-left = "Control+h";
        kb-row-right = "Control+l";
        kb-row-up = "Control+k";
        kb-row-down = "Control+j";
        kb-row-tab = "";
        kb-remove-char-back = "BackSpace";
        kb-remove-to-eol = "";
        kb-accept-entry = "Return,KP_Enter";
        kb-mode-complete = "";
      };
    };
    direnv = {
      enable = true;
      enableZshIntegration = true;
      nix-direnv = {
        enable = true;
      };
    };
    dircolors = {
      enable = true;
      enableZshIntegration = true;
      settings = {
        NORMAL = "00";
        FILE = "00";
        DIR = "01;38;5;245";
        LINK = "38;5;252";
        FIFO = "38;5;252";
        SOCK = "38;5;252";
        DOOR = "38;5;252";
        BLK = "38;5;252";
        CHR = "38;5;252";
        ORPHAN = "38;5;252";
        EXEC = "38;5;252";
        STICKY = "38;5;252";
        STICKY_OTHER_WRITABLE = "38;5;252";
        ".tar" = "38;5;242";
        ".tbz" = "38;5;242";
        ".tgz" = "38;5;242";
        ".deb" = "38;5;242";
        ".zip" = "38;5;242";
        ".gz" = "38;5;242";
        ".avi" = "38;5;243";
        ".mp4" = "38;5;243";
        ".bmp" = "38;5;243";
        ".gif" = "38;5;243";
        ".jpg" = "38;5;243";
        ".jpeg" = "38;5;243";
        ".svg" = "38;5;243";
        ".fli" = "38;5;243";
        ".mng" = "38;5;243";
        ".mov" = "38;5;243";
        ".mpg" = "38;5;243";
        ".pcx" = "38;5;243";
        ".pbm" = "38;5;243";
        ".pgm" = "38;5;243";
        ".png" = "38;5;243";
        ".ppm" = "38;5;243";
        ".tga" = "38;5;243";
        ".tif" = "38;5;243";
        ".xbm" = "38;5;243";
        ".xpm" = "38;5;243";
        ".dl" = "38;5;243";
        ".gl" = "38;5;243";
        ".wmv" = "38;5;243";
        ".aiff" = "38;5;243";
        ".au" = "38;5;243";
        ".mid" = "38;5;243";
        ".mp3" = "38;5;243";
        ".ogg" = "38;5;243";
        ".voc" = "38;5;243";
        ".wav" = "38;5;243";
      };
    };
  };

  services = {
    dunst = {
      enable = true;
      settings = {
        shortcuts = {
          close = "ctrl+space";
          close_all = "ctrl+shift+space";
          history = "ctrl+grave";
          context = "ctrl+shift+period";
        };
        urgency_low = {
          background = "#222222";
          foreground = "#888888";
          timeout = 10;
        };
        urgency_normal = {
          background = "#285577";
          foreground = "#ffffff";
          timeout = 10;
        };
        urgency_critical = {
          background = "#900000";
          foreground = "#ffffff";
          frame_color = "#ff0000";
          timeout = 0;
        };
        global = {
          monitor = 0;
          follow = "mouse";
          geometry = "300x5-30+20";
          indicate_hidden = "yes";
          shrink = "no";
          transparency = 0;
          notification_height = 0;
          separator_height = 2;
          padding = 8;
          horizontal_padding = 8;
          frame_width = 3;
          frame_color = "#aaaaaa";
          separator_color = "frame";
          sort = "yes";
          idle_threshold = 120;
          font = "Monospace 8";
          line_height = 0;
          markup = "full";
          format = "<b>%s</b>\n%b";
          alignment = "left";
          show_age_threshold = 60;
          word_wrap = "yes";
          ellipsize = "middle";
          ignore_newline = "no";
          stack_duplicates = true;
          hide_duplicate_count = false;
          show_indicators = "yes";
          icon_position = "off";
          max_icon_size = 32;
          sticky_history = "yes";
          history_length = 20;
          always_run_script = true;
          title = "Dunst";
          class = "Dunst";
          startup_notification = false;
          verbosity = "mesg";
          corner_radius = 0;
          force_xinerama = false;
          mouse_left_click = "close_current";
          mouse_middle_click = "do_action";
          mouse_right_click = "close_all";
        };
      };
    };
    gpg-agent = {
      enable = true;
      pinentryPackage = pkgs.pinentry-curses;
      defaultCacheTtl = 86400;
      maxCacheTtl = 86400;
    };
    picom = {
      enable = true;
      fade = true;
      fadeSteps = [ 0.05 0.03 ];
      inactiveOpacity = 1.0;
      settings = {
        blur-kern = "3x3box";
      };
    };
    unclutter.enable = true;
    betterlockscreen.enable = true;
    spotifyd = {
      enable = true;
      settings = {
        global = {
          username = "rory.hayford@protonmail.ch";
          password_cmd = "pass spotify";
          device_name = "nixos";
        };
      };
    };
    network-manager-applet.enable = true;
    blueman-applet.enable = true;
    clipmenu = {
      enable = true;
      launcher = "rofi";
    };
    flameshot.enable = true;
  };

  gtk = {
    enable = true;
    font.name = "Dejavu Sans 11";
    theme = {
      package = pkgs.arc-theme;
      name = "Arc-Dark";
    };
    iconTheme = {
      package = pkgs.arc-icon-theme;
      name = "arc-icon-theme";
    };
  };

  xresources.extraConfig = ''
    !! Colorscheme

    ! special
    *.foreground: #bababa
    *.background: #141c21
    *.cursorColor: #afbfbf

    ! black
    *.color0: #1d2021
    *.color8: #4c545c
    ! red
    *.color1: #ae5e5e
    *.color9: #cd5c5c
    ! green
    *.color2: #8fbc8f
    *.color10: #bdebb7
    ! yellow
    *.color3: #d3d382
    *.color11: #c8bc60
    ! blue
    *.color4: #7ca4c3
    *.color12: #b7d4f1
    ! magenta
    *.color5: #9ca1ff
    *.color13: #b3b2f9
    ! cyan
    *.color6: #85add4
    *.color14: #a4e3cb
    ! white
    *.color7: #bfbaac
    *.color15: #fdf6e3

    Xcursor.size: 16
  '';

  xsession = {
    enable = true;
  };
}
