{ config, pkgs, lib, ... }:

{
  imports = [
    ../config/wm/xmonad.nix
  ];

  home.stateVersion = "22.11";

  programs.autorandr = {
    enable = true;
    profiles = {
      "laptop" = {
        config = {
          HDMI-1.enable = false;
          DP-1.enable = false;
          DP-2.enable = false;
          DP-3.enable = false;
          eDP-1 = {
            enable = true;
            crtc = 0;
            mode = "1920x1080";
            primary = true;
            rate = "60.00";
            position = "0x0";
          };
        };
        fingerprint = {
          eDP-1 = lib.concatStrings
            [
              "00ffffffffffff0009e55c0a00000000151f0104a526157802f7e2ad5044b02"
              "40e525600000001010101010101010101010101010101c03980187138284030"
              "2036007ed71000001a342e801871382840302036007ed71000001a000000fe0"
              "04847503130804e5631374e344c0000000000024121b2001000000a410a2020"
              "0061"
            ];
        };
      };
    };
    hooks = {
      postswitch = {
        # DESATURATE IT
        desaturation = ''
          if [ $AUTORANDR_CURRENT_PROFILE = 'laptop' ]; then
            xrandr --output eDP-1 --gamma 1.2:1.2:1.2 --brightness 1.1
          fi
        '';
      };
    };
  };
}
