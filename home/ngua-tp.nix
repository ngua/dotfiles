{ config, pkgs, lib, ... }:

{
  imports = [
    ../config/wm/xmonad.nix
  ];

  home.stateVersion = "21.11";

  programs.autorandr = {
    enable = true;
    profiles = {
      "laptop" = {
        config = {
          HDMI-A-0.enable = false;
          DisplayPort-0.enable = false;
          DisplayPort-1.enable = false;
          eDP = {
            enable = true;
            crtc = 0;
            mode = "1920x1080";
            primary = true;
            rate = "60.00";
            position = "0x0";
          };
        };
        fingerprint = {
          eDP = lib.concatStrings
            [
              "00ffffffffffff0009e5de0900000000201e0104a51f1178035ff5965d5592291d5054"
              "000000010101010101010101010101010101018540802c7138a0403020360035ae1000"
              "001a000000fd00283c4a4a11010a202020202020000000fe00424f452043510a202020"
              "202020000000fe004e5631343046484d2d4e34560a00a5"
            ];
        };
      };
      "sptv" = {
        config = {
          eDP.enable = false;
          DisplayPort-0.enable = false;
          DisplayPort-1.enable = false;
          HDMI-A-0 = {
            enable = true;
            crtc = 1;
            mode = "1920x1080";
            primary = false;
            rate = "120.00";
            position = "0x0";
          };
        };
        fingerprint = {
          HDMI-A-0 = lib.concatStrings
            [
              "00ffffffffffff001e6d010001010101011d010380a05a780aee91a3544c99260f5054"
              "a1080031404540614071408180d1c00101010104740030f2705a80b0588a0040846300"
              "001e023a801871382d40582c450040846300001e000000fd0018781e871e000a202020"
              "202020000000fc004c472054560a20202020202020018c02034bf1565f101f04130514"
              "03021220212215015d5e6263643f402f0957071507505507003d1ec05f7e016e030c00"
              "1000b83c20008001020304e200cfe305c000e50e60616566e3060d01662150b051001b"
              "304070360040846300001e000000000000000000000000000000000000000000000000"
              "0000000000000000000019"
            ];
          eDP = lib.concatStrings
            [
              "00ffffffffffff0009e5de0900000000201e0104a51f1178035ff5965d5592291d5054"
              "000000010101010101010101010101010101018540802c7138a0403020360035ae1000"
              "001a000000fd00283c4a4a11010a202020202020000000fe00424f452043510a202020"
              "202020000000fe004e5631343046484d2d4e34560a00a5"
            ];
        };
      };
    };
    hooks = {
      postswitch = {
        # hacky way to set brightness
        "set-brightness" = ''
          if [ $AUTORANDR_CURRENT_PROFILE = 'sptv' ]; then
            xrandr --output HDMI-A-0 --brightness 1.2
          fi
        '';
      };
    };
  };
}
