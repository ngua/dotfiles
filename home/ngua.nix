args@{ config, pkgs, lib, ... }:

{
  imports = [
    ../config/wm/xmonad.nix
  ];

  programs = {
    autorandr = {
      enable = true;
      profiles = {
        "laptop" = {
          config = {
            DP-1.enable = false;
            DP-2.enable = false;
            eDP-1 = {
              enable = true;
              crtc = 0;
              mode = "1920x1280";
              primary = true;
              rate = "60.00";
              position = "0x0";
              gamma = "0.8:0.8:0.8";
            };
          };
          fingerprint = {
            eDP-1 = lib.concatStrings
              [
                "00ffffffffffff004d10791400000000311a0104a51a11780e0730a4544c9a260e5054000"
                "00001010101010101010101010101010101d04180a07000465030203a0003ad10000018d0"
                "4180a07000975130203a0003ad10000018000000fe00564b4a434e814c513132334e31000"
                "000000002410328011200000b010a20200071"
              ];
          };
        };
        "tivi" = {
          config = {
            eDP-1.enable = false;
            DP-2.enable = false;
            DP-1 = {
              enable = true;
              crtc = 1;
              mode = "1920x1080";
              primary = false;
              rate = "60.00";
              position = "0x0";
            };
          };
          fingerprint = {
            DP-1 = lib.concatStrings
              [
                "00ffffffffffff004dd9047c01010101011c0103806c3d780a0dc9a05747982712484c210"
                "8008180a9c0714fb300010101010101010104740030f2705a80b0588a003d624200001e02"
                "3a801871382d40582c45003d624200001e000000fc00534f4e5920545620202a30300a000"
                "000fd00303e0e461e000a202020202020015702034df0575d5e5f621f101405130420223c"
                "3e1216030711150206012c097f071507503d07bc570600830f00006e030c003000b83c2f0"
                "08001020304e200f9e305ff01e50e60616566e3060d01011d007251d01e206e2855003d62"
                "4200001e0000000000000000000000000000000000000000000000000000000000000000ed"
              ];
          };
        };
      };
      hooks = {
        postswitch = {
          # hacky way to set brightness
          "set-brightness" = ''
            if [ $AUTORANDR_CURRENT_PROFILE = 'laptop' ]; then
              xrandr --output eDP-1 --brightness 1.2
            fi
          '';
        };
      };
    };
  };
}
