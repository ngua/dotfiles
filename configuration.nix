{ pkgs, lib, inputs, host, ... }:

{
  imports = [
    ./packages
    ./hosts
    ./config/zsh.nix
    (./. + "/hardware/${host.name}.nix")
  ] ++ lib.lists.optional
    (host.graphical or true)
    ./modules/graphical.nix;

  nix = {
    extraOptions = ''
      experimental-features = nix-command flakes recursive-nix impure-derivations ca-derivations
      keep-outputs = true
      keep-derivations = true

      warn-dirty = false
      allow-import-from-derivation = true
    '';

    package = lib.mkForce pkgs.nixVersions.nix_2_19;

    registry.nixpkgs.flake = inputs.nixpkgs;

    settings = {
      trusted-users = [ "${host.user}" "root" ];
      substituters = [
        "https://nix-community.cachix.org/"
      ];
      trusted-public-keys = [
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      ];

      auto-optimise-store = true;
    };

    # gc = {
    #   automatic = true;
    #   dates = "Sun 22:00";
    # };
  };

  nixpkgs.config = {
    allowUnfree = true;
    permittedInsecurePackages = [
      "nix-2.15.3"
    ];
  };

  environment = {
    systemPackages = with pkgs; [
      coreutils
      powertop
      vim
      curl
      wget
      ripgrep
      git
      sshfs
      tree
      which
      whois
      rsync
      gdb
      pciutils
      usbutils
      lshw
      pinentry-curses
      findutils
      cachix
      docker-compose
      file
      acpi
    ];
  };

  hardware = {
    enableRedistributableFirmware = true;
  };

  programs = {
    less = {
      enable = true;
      envVariables = { LESS = "-R -i -J -W"; };
    };
    ssh.askPassword = "";
  };

  services = {
    earlyoom.enable = true;
    upower.enable = true;
  };

  console = {
    packages = [ pkgs.terminus_font ];
    earlySetup = true;
    font = "ter-v32n";
  };

  security = {
    sudo.extraConfig = ''
      Defaults timestamp_timeout=60
    '';
    polkit.enable = true;
  };

  virtualisation = {
    docker = {
      enable = true;
      storageDriver = "overlay2";
    };
  };

  networking.nameservers = [ "1.1.1.1" "8.8.8.8" ];

  users.users.${host.user} = {
    isNormalUser = true;
    home = "/home/${host.user}";
    extraGroups = [ "wheel" "audio" "video" "docker" ];
    uid = 1000;
    shell = pkgs.zsh;
  };
}
