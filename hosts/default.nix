{ lib, host, ... }:

{
  imports =
    let
      arch = builtins.elemAt (lib.strings.split "-" host.system) 0;
    in
    [
      (./. + "/${host.name}.nix")
      (../. + "/modules/${host.fsType}.nix")
      (../. + "/modules/${arch}.nix")
    ];

  time.timeZone = "Asia/Saigon";

  # Always choose a single consistent name for wireless interface
  systemd.network.links."10-wlan0" = {
    matchConfig.PermanentMACAddress = host.wireless;
    linkConfig.Name = "wlan0";
  };

}
