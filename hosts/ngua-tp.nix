{ lib, host, ... }:

{
  networking = {
    hostName = "ngua";
    hostId = "c54c5c03";
    networkmanager.enable = true;
  };

  services = {
    kmonad = {
      enable = true;
      keyboards = lib.kmonads
        [
          {
            name = "laptop";
            input = "platform-i8042-serio-0-event-kbd";
            cfg = ../config/kmonad/t14.kbd;
          }
          {
            name = "leopold-usb";
            input = "pci-0000:07:00.4-usb-0:2:1.0-event-kbd";
            cfg = ../config/kmonad/leopold.kbd;
          }
        ];
    };
  };

  packages = {
    libvirt.enable = true;
    adb.enable = true;
    dev = {
      haskell.enable = true;
      nix.enable = true;
      misc.enable = true;
      sh.enable = true;
    };
  };

  users.users.${host.user}.extraGroups = [ "networkmanager" ];

  system.stateVersion = "21.11";
}
