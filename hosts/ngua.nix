{
  services = {
    xserver.wacom.enable = true;
  };

  hardware = {
    cpu.intel.updateMicrocode = true;
  };

  networking = {
    hostName = "ngua";
    wireless.iwd.enable = true;
  };

  packages = {
    libvirt.enable = true;
    adb.enable = true;
    dev = {
      haskell.enable = true;
      nix.enable = true;
      misc.enable = true;
      sh.enable = true;
    };
  };

  systemd.services.iwd.serviceConfig.Restart = "always";

  system.stateVersion = "20.09";
}
