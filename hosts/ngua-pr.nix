{ lib, host, ... }:

{
  networking = {
    hostName = "ngua";
    hostId = "c28aacbf";
    networkmanager.enable = true;
  };

  services = {
    kmonad = {
      enable = true;
      keyboards = lib.kmonads
        [
          {
            name = "laptop";
            input = "platform-i8042-serio-0-event-kbd";
            cfg = ../config/kmonad/precision.kbd;
          }
          {
            name = "leopold-usb";
            input = "pci-0000:00:14.0-usb-0:8:1.0-event-kbd";
            cfg = ../config/kmonad/leopold.kbd;
          }
        ];
    };
  };

  packages = {
    libvirt.enable = true;
    adb.enable = true;
    dev = {
      haskell.enable = true;
      nix.enable = true;
      misc.enable = true;
      sh.enable = true;
    };
  };

  users.users.${host.user}.extraGroups = [ "networkmanager" ];

  system.stateVersion = "22.11";
}
