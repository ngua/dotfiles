{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/release-24.05";
    home-manager.url = "github:nix-community/home-manager/release-24.05";
    nixos-hardware.url = "github:NixOS/nixos-hardware";
    nur.url = "github:nix-community/NUR";
    emacs-overlay.url = "github:nix-community/emacs-overlay/a798b734eeea4177bc9359aa2b1bbb5e8b368acc";
    doom-emacs = {
      url = "github:marienz/nix-doom-emacs-unstraightened/819ccd979bb0eaf66015e42a5550f6f0d2babea4";
      inputs.emacs-overlay.follows = "emacs-overlay";
    };
    ragenix.url = "github:yaxitech/ragenix";
    kmonad.url = "github:kmonad/kmonad?dir=nix";
    work-modules = {
      url = "git+ssh://git@github.com/ngua/work-modules";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    { nixpkgs
    , home-manager
    , nixos-hardware
    , nur
    , doom-emacs
    , kmonad
    , work-modules
    , ...
    }@inputs:
    let
      overlays = [
        (_: _: { inherit inputs; })
        (import ./overlays)
        nur.overlay
      ];
    in
    {
      legacyPackages.x86_64-linux = import nixpkgs {
        inherit overlays;
        system = "x86_64-linux";
      };

      nixosConfigurations =
        let
          mkNixosSystem =
            { host
            , system ? "x86_64-linux"
            , extraModules ? [ ]
            }:
            nixpkgs.lib.nixosSystem {
              inherit (host) system;
              specialArgs = {
                inherit inputs host;
                # Extend `lib` with some nonsense to avoid needing to come up
                # with another `specialArg`
                lib = nixpkgs.lib.extend (import ./lib);
              };
              modules = extraModules ++ [
                ./configuration.nix
                kmonad.nixosModules.default
                home-manager.nixosModules.home-manager
                work-modules.nixosModules.default

                {
                  nixpkgs = {
                    inherit overlays;
                  };
                  home-manager = {
                    useUserPackages = true;
                    useGlobalPkgs = true;
                    backupFileExtension = "bak.hm";
                    users.${host.user} = nixpkgs.lib.mkMerge [
                      doom-emacs.hmModule
                      { imports = [ ./home ]; }
                    ];
                    extraSpecialArgs = { inherit host inputs; };
                  };
                }
              ];
            };
        in
        {
          ngua-tp = mkNixosSystem {
            host = {
              name = "ngua-tp";
              user = "rory";
              system = "x86_64-linux";
              fsType = "zfs";
              backlight = "amdgpu_bl0";
              fontsize = 8;
              # FIXME
              wireless = "14:75:5b:5a:db:e2";
            };
            extraModules = [
              nixos-hardware.nixosModules.lenovo-thinkpad-t14-amd-gen2
            ];
          };

          ngua-pr = mkNixosSystem {
            host = {
              name = "ngua-pr";
              user = "rory";
              system = "x86_64-linux";
              fsType = "zfs";
              backlight = "intel_backlight";
              fontsize = 10;
              wireless = "14:75:5b:5a:db:e2";
            };
            extraModules = [
              nixos-hardware.nixosModules.common-cpu-intel
            ];
          };

          ngua = mkNixosSystem {
            host = {
              name = "ngua";
              user = "rory";
              system = "x86_64-linux";
              fsType = "xfs";
              backlight = "intel_backlight";
              fontsize = 8;
              # FIXME
              wireless = "14:75:5b:5a:db:e2";
            };
          };
        };

    };
}
