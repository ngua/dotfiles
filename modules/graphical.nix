{ pkgs, host, ... }:

{
  services = {
    xserver = {
      enable = true;
      desktopManager.xterm.enable = false;
      displayManager.defaultSession = "xsession";
      displayManager = {
        lightdm = {
          enable = true;
          background = ../config/lock/plant.jpg;
          greeters.mini = {
            user = "${host.user}";
            enable = true;
            extraConfig = builtins.readFile ../config/mini-greeter.conf;
          };
        };
        session = [
          {
            manage = "desktop";
            name = "xsession";
            start = ''
              exec $HOME/.xsession
            '';
          }
        ];
      };
      libinput = {
        enable = true;
        touchpad = {
          disableWhileTyping = true;
          tapping = false;
          clickMethod = "clickfinger";
        };
      };
    };
    # required to configure gtk3 w/ home-manager
    dbus.packages = [ pkgs.dconf ];
    blueman.enable = true;
  };

  sound.enable = true;

  hardware = {
    pulseaudio.enable = true;
    acpilight.enable = true;
    bluetooth.enable = true;
  };

  programs = {
    firejail.enable = true;
    light.enable = true;
    dconf.enable = true;
  };

  qt = {
    enable = true;
    platformTheme = "gnome";
    style = "adwaita-dark";
  };

  fonts = {
    packages = with pkgs; [
      fira-code
      fira-code-symbols
      fira
      cantarell-fonts
      noto-fonts
      noto-fonts-extra
      noto-fonts-cjk
      noto-fonts-emoji
      dejavu_fonts
      liberation_ttf
      siji
      doulos-sil
      font-awesome_5
      (
        nerdfonts.override {
          fonts = [ "NerdFontsSymbolsOnly" "FiraCode" ];
        }
      )
    ];
  };

  systemd.user.services = {
    lock-on-sleep = {
      unitConfig = {
        Description = "Lock the screen upon lid close";
      };
      serviceConfig = {
        ExecStart = "${pkgs.betterlockscreen}/bin/betterlockscreen -l";
      };
      wantedBy = [
        "pre-sleep.service"
      ];
      environment = {
        DISPLAY = ":0";
        XAUTHORITY = "$HOME/.Xauthority";
      };
    };
  };

}
