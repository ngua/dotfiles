{ ... }:

{
  boot = {
    binfmt.emulatedSystems = [ "aarch64-linux" "armv7l-linux" ];
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
  };
}
