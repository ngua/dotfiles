{ config, lib, pkgs, ... }:

let
  cfg = config.packages.dev.misc;
in
{
  options.packages.dev.misc.enable = lib.mkEnableOption "Misc. tools";

  config = lib.mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      openocd
    ];
  };
}
