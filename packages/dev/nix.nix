{ config, lib, pkgs, ... }:

let
  cfg = config.packages.dev.nix;
in
{
  options.packages.dev.nix.enable = lib.mkEnableOption "Nix tools";

  config = lib.mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      nixpkgs-fmt
      nil
    ];
  };
}
