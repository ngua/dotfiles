{ config, lib, pkgs, ... }:

let
  cfg = config.packages.dev.haskell;
in
{
  options.packages.dev.haskell.enable = lib.mkEnableOption "Haskell tools";

  config = lib.mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      haskellPackages.cabal-fmt
      haskellPackages.implicit-hie
    ];
  };
}
