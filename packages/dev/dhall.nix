{ config, lib, pkgs, ... }:

let
  cfg = config.packages.dev.dhall;
in
{
  options.packages.dev.dhall.enable = lib.mkEnableOption "Dhall tools";

  config = lib.mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      dhall
      dhall-json
      dhall-bash
      dhall-lsp-server
    ];
  };
}
