{ config, lib, pkgs, ... }:

let
  cfg = config.packages.dev.sh;
in
{
  options.packages.dev.sh.enable = lib.mkEnableOption "Bash/shell tools";

  config = lib.mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      nodePackages.bash-language-server
      shfmt
      shellcheck
    ];
  };
}
