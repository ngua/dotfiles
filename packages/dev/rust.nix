{ config, lib, pkgs, ... }:

let
  cfg = config.packages.dev.rust;
in
{
  options.packages.dev.rust.enable = lib.mkEnableOption "Rust tools";

  config = lib.mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      rustfmt
      rustup
    ];
  };
}
