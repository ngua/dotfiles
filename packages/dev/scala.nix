{ config, lib, pkgs, ... }:

let
  cfg = config.packages.dev.scala;
in
{
  options.packages.dev.scala.enable = lib.mkEnableOption "Scala tools";

  config = lib.mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      scala
      sbt
      jdk
      scalafmt
    ];
  };
}
