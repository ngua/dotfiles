{ config, lib, host, ... }:

let
  cfg = config.packages.transmission;
in
{
  options.packages.transmission.enable =
    lib.mkEnableOption "Transmission daemon";

  config = lib.mkIf cfg.enable {
    services.transmission = { enable = true; };
    users.users.${host.user}.extraGroups = [ "transmission" ];
  };
}
