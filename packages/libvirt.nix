{ config, lib, pkgs, host, ... }:

let
  cfg = config.packages.libvirt;
in
{
  options.packages.libvirt.enable = lib.mkEnableOption "libvirt/qemu";

  config = lib.mkIf cfg.enable {
    virtualisation.libvirtd = {
      enable = true;
      qemu.ovmf.enable = true;
    };

    environment.systemPackages = [ pkgs.virt-manager pkgs.qemu-utils ];

    users.users.${host.user}.extraGroups = [ "libvirtd" ];
  };
}
