{ config, lib, pkgs, host, ... }:

let
  cfg = config.packages.adb;
in
{
  options.packages.adb.enable = lib.mkEnableOption "ADB";

  config = lib.mkIf cfg.enable {
    programs = {
      adb.enable = true;
      fuse.userAllowOther = true;
    };
    users.users.${host.user}.extraGroups = [ "adbusers" ];
    environment.systemPackages = with pkgs; [
      mtpfs
      libmtp
    ];
  };
}
