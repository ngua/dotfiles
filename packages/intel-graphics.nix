{ config, lib, pkgs, ... }:

let
  cfg = config.packages.intel-graphics;
in
{
  options.packages.intel-graphics.enable = lib.mkEnableOption "intel-graphics";

  config = lib.mkIf cfg.enable {
    hardware.opengl = {
      enable = true;
      extraPackages = with pkgs; [
        intel-media-driver # LIBVA_DRIVER_NAME=iHD
        vaapiVdpau
        libvdpau-va-gl
      ];
    };
  };
}
