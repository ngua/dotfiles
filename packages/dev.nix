{ ... }:

{
  imports = [
    ./dev/haskell.nix
    ./dev/misc.nix
    ./dev/nix.nix
    ./dev/dhall.nix
    ./dev/sh.nix
    ./dev/scala.nix
    ./dev/rust.nix
  ];
}
