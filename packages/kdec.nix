{ config, lib, pkgs, ... }:

let
  cfg = config.packages.kdec;
in
{
  options.packages.kdec.enable = lib.mkEnableOption "KDE Connect";

  config = lib.mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      kdeconnect
    ];
    networking.firewall = {
      allowedUDPPortRanges = [
        {
          from = 1714;
          to = 1764;
        }
      ];
      allowedTCPPortRanges = [
        {
          from = 1714;
          to = 1764;
        }
      ];
    };
  };
}
