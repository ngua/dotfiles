#!/usr/bin/env bash

workspaces() {
    declare -a widgets

    for workspace in $(wmctrl -d | awk '{ print $1 $2 }'); do
        local ws="${workspace:0:1}"
        local class="unfocused"
        local icon=""
        if [ "${workspace:1:2}" = '*' ]; then
            class="focused"
        fi
        local widget="(eventbox :cursor \"hand\" (button :class \"$class\" :onclick \"wmctrl -s $ws\" \"$icon\"))"
        widgets+=("$widget")
    done
    echo "(box :class \"workspaces\" :halign \"center\" :valign \"center\" :vexpand true :hexpand true ${widgets[*]})"
}

workspaces
while true; do
    sleep 0.3 && workspaces
done
