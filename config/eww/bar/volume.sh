#!/usr/bin/env bash

sink=0
getmuted="$(pactl get-sink-mute $sink | awk -F: '{ print $2 }' | tr -d ' ')"
getvol="$(pactl get-sink-volume 0 | awk -F '/' '{ print $2 }' | tr -d '% ')"
icon=""
label=""
muted=false

if [[ "$getmuted" = "yes" ]]; then
    icon=""
    label="X"
    muted=true
else
    icon=""
    label="$getvol"
fi

jo icon="$icon" label="$label" muted="$muted"
