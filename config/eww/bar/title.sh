#!/usr/bin/env bash

wname=$(xdotool getwindowfocus getwindowname)
displayed="$wname"
limit=30

if [ "${#wname}" -gt "$limit" ]; then
    displayed="$(echo "$wname" | cut -c 1-"$limit")..."
fi

echo "$displayed"
