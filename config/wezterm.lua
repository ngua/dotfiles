local wezterm = require 'wezterm';

-- Borrowed from: https://github.com/protiumx/.dotfiles/blob/main/stow/wezterm/.config/wezterm/wezterm.lua
local function get_current_working_dir(tab)
  local current_dir = tab.active_pane and tab.active_pane.current_working_dir or { file_path = '' }
  local HOME_DIR = os.getenv('HOME')

  return current_dir.file_path == HOME_DIR and '~'
    or string.gsub(current_dir.file_path, '(.*[/\\])(.*)', '%2')
end

local function get_process(tab)
  if not tab.active_pane or tab.active_pane.foreground_process_name == '' then
    return nil
  end

  local process_name = string.gsub(tab.active_pane.foreground_process_name, '(.*[/\\])(.*)', '%2')
  return string.format('[%s]', process_name)
end


wezterm.on("format-tab-title", function(tab, tabs, panes, config, hover, max_width)
  local cwd = wezterm.format({
    { Text = get_current_working_dir(tab) },
  })
  local process = get_process(tab)
  local title = process and string.format(' %s %s ', process, cwd) or ' [?] '

  return {
    { Text = title },
  }
end)

return {
  font = wezterm.font("Fira Code", { weight = "Light" }),
  font_size = 14;
  window_padding = {
    left = 16,
    right = 16,
    top = 16,
    bottom = 16
  },
  colors = {
    foreground = "#f5f5f5",
    background = "#262626",
    cursor_bg = "#f5f5f5",
    cursor_fg = "#262626",
    cursor_border = "#f5f5f5",
    selection_fg = "#262626",
    selection_bg = "#f5f5f5",
    ansi = {"#6f6f6f", "#e28e8e", "#8fbc8f", "#d3d382", "#7ca4c3", "#9ca1ff", "#85add4", "#f5f5f5"},
    brights = {"#6f6f6f", "#e28e8e", "#8fbc8f", "#d3d382", "#7ca4c3", "#9ca1ff", "#85add4", "#f5f5f5"},

    tab_bar = {
      active_tab = {
        bg_color = "#262626",
        fg_color = "#f5f5f5"
      },
      inactive_tab = {
        bg_color = "#333333",
        fg_color = "#7c7c7c"
      },
    }
  },

  keys = {
    {
      key = 'Enter',
      mods = 'CTRL|SHIFT',
      action = wezterm.action.SpawnWindow,
    },
    {
      key = 'j',
      mods = 'CTRL|SHIFT',
      action = wezterm.action.ActivateTabRelative(-1),
    },
    {
      key = 'k',
      mods = 'CTRL|SHIFT',
      action = wezterm.action.ActivateTabRelative(1),
    },
  },

  enable_tab_bar = true,
  hide_tab_bar_if_only_one_tab = true,
  enable_scroll_bar = false,
  check_for_updates = false,
  show_update_window = false,
  window_close_confirmation = "NeverPrompt",
  adjust_window_size_when_changing_font_size = false,
}
