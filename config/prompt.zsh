autoload -Uz promptinit && promptinit
autoload -Uz vcs_info
autoload -U colors && colors
setopt PROMPT_SUBST

precmd() {
    vcs_info
}

_fix_cursor() {
    echo -ne "\e[6 q"
}

nix_shell_status() {
    [[ -z "${IN_NIX_SHELL}" ]] || echo '❅ '
}

precmd_functions+=(_fix_cursor)

zstyle ':vcs_info:*' enable git hg
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:git*:*' unstagedstr '*'
zstyle ':vcs_info:git*:*' stagedstr '✓'
zstyle ':vcs_info:git*' \
    formats \
    "%{${fg[white]}%}%{${fg[green]}%}%b%%{${fg[white]}%} %B%{${fg[yellow]}%}%m%u%c%%b%{${fg[white]}%} %{${fg[white]}%}%{$reset_color%}"

PROMPT='$(nix_shell_status)[%B%F{white}%m%f%b]:%B%F{67}%(4~|.../%3~|%~)%f%b %(!.#.%%) '
RPROMPT=$'${vcs_info_msg_0_}'
