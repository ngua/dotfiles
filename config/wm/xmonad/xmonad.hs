{-# LANGUAGE NamedFieldPuns #-}

module Main where

import Data.Bool (bool)
import Graphics.X11.ExtraTypes.XF86
  ( xF86XK_AudioLowerVolume,
    xF86XK_AudioMute,
    xF86XK_AudioRaiseVolume,
    xF86XK_MonBrightnessDown,
    xF86XK_MonBrightnessUp,
  )
import System.Exit (exitSuccess)
import XMonad
import XMonad.Hooks.DynamicLog (statusBar)
import XMonad.Hooks.EwmhDesktops (addEwmhWorkspaceSort, ewmh)
import XMonad.Hooks.InsertPosition
  ( Focus (Newer),
    Position (Below),
    insertPosition,
  )
import XMonad.Hooks.ManageHelpers (doFullFloat, isFullscreen)
import XMonad.Layout.Groups.Helpers
  ( focusDown,
    focusUp,
    swapMaster,
  )
import XMonad.Layout.Spacing
  ( Border (Border),
    spacingRaw,
    toggleScreenSpacingEnabled,
    toggleWindowSpacingEnabled,
  )
import XMonad.Operations (windows)
import XMonad.StackSet (RationalRect (RationalRect), greedyView)
import XMonad.Util.EZConfig (additionalKeys)
import XMonad.Util.NamedScratchpad
  ( NamedScratchpad (NS),
    customFloating,
    namedScratchpadAction,
    namedScratchpadManageHook,
    scratchpadWorkspaceTag,
  )
import XMonad.Util.SpawnOnce (spawnOnce)
import XMonad.Util.WorkspaceCompare (filterOutWs)

main :: IO ()
main =
  xmonad
    . addEwmhWorkspaceSort
      ( pure
          (filterOutWs [scratchpadWorkspaceTag])
      )
    . ewmh
    =<< bar config
  where
    config =
      def
        { borderWidth = 2
        , modMask = mod4Mask
        , terminal = term
        , normalBorderColor = "#333333"
        , focusedBorderColor = "#989898"
        , manageHook
        , layoutHook
        , startupHook
        }
        `additionalKeys` kbds

    manageHook =
      (isFullscreen --> doFullFloat)
        <+> insertPosition Below Newer
        <+> namedScratchpadManageHook scratchpads

    -- If a window isn't spawned on the first workspace, the struts with eww
    -- don't work
    -- There's almost certainly a real way to fix this, but this is a fast
    -- hack
    startupHook = mconcat [spawnOnce "sleep 1"]

    layoutHook =
      spacingRaw False (Border 5 0 5 0) True (Border 0 5 0 5) True $
        Tall 1 (3 / 100) (1 / 2) ||| Full

    bar = statusBar "eww open bar" def toggleStrutsKey

    toggleStrutsKey XConfig{modMask = modm} = (modm, xK_b)

kbds :: [((KeyMask, KeySym), X ())]
kbds =
  [ ((mod4Mask, xK_e), spawn "emacs")
  , ((mod4Mask, xK_h), focusUp)
  , ((mod4Mask, xK_l), focusDown)
  , ((mod4Mask .|. shiftMask, xK_h), swapMaster)
  , ((mod4Mask, xK_r), sendMessage Shrink)
  , ((mod4Mask .|. shiftMask, xK_r), sendMessage Expand)
  , ((mod4Mask, xK_Return), spawn term)
  , ((mod4Mask .|. shiftMask, xK_q), kill)
  , ((mod4Mask .|. shiftMask, xK_l), spawn "betterlockscreen -l")
  , ((mod4Mask .|. shiftMask, xK_e), io exitSuccess)
  ,
    ( (mod4Mask, xK_s)
    , toggleScreenSpacingEnabled >> toggleWindowSpacingEnabled
    )
  , ((mod4Mask, xK_d), spawn "rofi -show run")
  , ((mod4Mask, xK_c), spawn "clipmenu")
  ,
    ( (mod4Mask, xK_g)
    , spawn "exec firejail --noexec=/tmp --whitelist=~/.config/tridactyl --whitelist=~/.emacs firefox"
    )
  , ((0, xF86XK_AudioMute), spawn "pactl -- set-sink-mute 0 toggle")
  , ((0, xF86XK_AudioRaiseVolume), spawn "pactl -- set-sink-volume 0 +5%")
  , ((0, xF86XK_AudioLowerVolume), spawn "pactl -- set-sink-volume 0 -5%")
  , ((0, xF86XK_MonBrightnessUp), spawn "light -A 10")
  , ((0, xF86XK_MonBrightnessDown), spawn "light -U 10")
  ,
    ( (mod4Mask, xK_Insert)
    , spawn "scrot -u '%Y-%m-%d_%H:%M:%S.png' -e 'mv $f /tmp/'"
    )
  ,
    ( (mod4Mask .|. shiftMask, xK_Insert)
    , spawn "scrot -s '%Y-%m-%d_%H:%M:%S.png' -e 'mv $f /tmp/'"
    )
  ,
    ( (mod4Mask .|. controlMask, xK_Insert)
    , spawn "maim -s --format=png /dev/stdout | xclip -sel clip -t image/png -i"
    )
  ,
    ( (mod4Mask .|. shiftMask, xK_Return)
    , namedScratchpadAction scratchpads "term"
    )
  , -- HACK
    -- Treat workspace 1 as a "hidden" workspace to work around issues with
    -- `wmctrl -d` not picking up focus change when using eww as a status bar
    ((mod4Mask, xK_0), windows $ greedyView "1")
  , ((mod4Mask, xK_1), windows $ greedyView "2")
  , ((mod4Mask, xK_2), windows $ greedyView "3")
  , ((mod4Mask, xK_3), windows $ greedyView "4")
  , ((mod4Mask, xK_4), windows $ greedyView "5")
  , ((mod4Mask, xK_5), windows $ greedyView "6")
  , ((mod4Mask, xK_6), windows $ greedyView "7")
  , ((mod4Mask, xK_7), windows $ greedyView "8")
  , ((mod4Mask, xK_8), windows $ greedyView "9")
  ]

term :: [Char]
term = "wezterm"

scratchpads :: [NamedScratchpad]
scratchpads =
  [ NS "term" cmd (className =? "scratch") . customFloating $
      RationalRect (1 / 6) (1 / 6) (2 / 3) (2 / 3)
  ]
  where
    cmd :: [Char]
    cmd = term <> "start --class=scratch -e zsh"
