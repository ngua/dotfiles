{ config, pkgs, lib, host, ... }:

{
  xsession = {
    windowManager.xmonad = {
      enable = true;
      enableContribAndExtras = true;
      config = ./xmonad/xmonad.hs;
    };
    initExtra = ''
      random-bg
      autorandr -c > /dev/null
      eww daemon
    '';
  };

  services.stalonetray = {
    enable = true;
    extraConfig = ''
      background "#262626"
      decorations none
      transparent false
      dockapp_mode none
      geometry 3x2-0+0
      kludges force_icons_size
      grow_gravity NE
      icon_gravity NE
      icon_size 18
      sticky true
      window_type dock
      window_layer bottom
      no_shrink false
      skip_taskbar true
    '';
  };
}
