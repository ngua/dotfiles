{ config, pkgs, ... }:

{
  programs.zsh = {
    enable = true;
    autosuggestions = {
      enable = true;
      highlightStyle = "fg=245";
    };
    histSize = 100000;
    setOptions = [
      "HIST_IGNORE_ALL_DUPS"
      "HIST_FIND_NO_DUPS"
      "HIST_IGNORE_SPACE"
      "AUTOCD"
      "CORRECT"
      "PROMPT_SUBST"
      "MENU_COMPLETE"
      "NO_LIST_AMBIGUOUS"
      "INTERACTIVECOMMENTS"
    ];
    shellAliases = {
      "sdn" = "echo ignored | xargs -p -I {} systemctl poweroff";
      "start" = "sudo systemctl start";
      "stop" = "sudo systemctl stop";
      "status" = "systemctl status";
      "ll" = "ls -lh --color -F";
      "la" = "ls -lha --color -F";
      "ldot" = "ls -ld .?*";
      "ls" = "ls --color -F";
    };
    promptInit = builtins.readFile ./prompt.zsh;
    syntaxHighlighting = {
      enable = true;
      styles = {
        "alias" = "fg=111";
        "suffix-alias" = "fg=10";
        "builtin" = "fg=111";
        "function" = "fg=blue,bold";
        "command" = "fg=blue,bold";
        "single-quoted-argument" = "fg=white,bold";
        "double-quoted-argument" = "fg=white,bold";
        "back-quoted-argument" = "fg=white,bold";
        "path" = "fg=white,bold";
        "precommand" = "fg=yellow,bold";
        "single-hyphen-option" = "fg=249";
        "double-hyphen-option" = "fg=249";
        "comment" = "fg=245";
      };
    };
    interactiveShellInit = ''
      # suffix aliases
      alias -s pdf=zathura
      alias -s {png,jpg,jpeg}=feh
      alias -s mp4=mpv
      alias -s {json,yaml,yml,dhall,nix,txt,md,tex}=$EDITOR

      zmodload -i zsh/complist
      zstyle ':completion:*' menu select 'm:{a-z}={A-Za-z}'
      zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'

      bindkey -v
      bindkey -M vicmd '?' history-incremental-search-backward
      bindkey -M vicmd '/' history-incremental-search-forward
      bindkey -M vicmd 'N' history-incremental-pattern-search-backward
      bindkey -M vicmd 'n' history-incremental-pattern-search-forward
      bindkey -M vicmd ' ' edit-command-line
      bindkey -M menuselect '^h' vi-backward-char
      bindkey -M menuselect '^k' vi-up-line-or-history
      bindkey -M menuselect '^l' vi-forward-char
      bindkey -M menuselect '^j' vi-down-line-or-history
      bindkey -M menuselect '^w' accept-search

      function zle-keymap-select() {
          if [ "$TERM" = "xterm-256color" ] || [ "$TERM" = "alacritty" ] || [ "$TERM" = "st-256color" ]; then
              if [ $KEYMAP = vicmd ]; then
                  # command mode
                  echo -ne "\e[2 q"
              elif [ $KEYMAP = main ] || [ $KEYMAP = viins ]  || [ $KEYMAP = ''' ] || [ $KEYMAP = 'beam' ]; then
                  # insert mode
                  echo -ne "\e[6 q"
              fi
          fi
      }

      zle -N edit-command-line
      zle -N zle-keymap-select

      function __jq() {
          jq "$@" -C | less -F
      }

      alias jq='__jq'

      export LESS_TERMCAP_md=$'\e[01;37m'
      export LESS_TERMCAP_me=$'\e[0m'
      export LESS_TERMCAP_se=$'\e[0m'
      export LESS_TERMCAP_so=$'\e[01;37;93m'
      export LESS_TERMCAP_ue=$'\e[0m'
      export LESS_TERMCAP_us=$'\e[01;34m'
    '';
  };

}
