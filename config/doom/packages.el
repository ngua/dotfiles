;; -*- no-byte-compile: t; -*-
;;; $DOOMDIR/packages.el

(package! dhall-mode)
(package! rescript-mode)
(package! exec-path-from-shell)
(package! engrave-faces)
(package! anki-editor)
