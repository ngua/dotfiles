(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#282b33" "#C16069" "#A2BF8A" "#ECCC87" "#80A0C2" "#B58DAE" "#86C0D1" "#eceff4"])
 '(ansi-term-color-vector
   [unspecified "#2e2e2e" "#bc8383" "#7f9f7f" "#d0bf8f" "#6ca0a3" "#dc8cc3" "#8cd0d3" "#b6b6b6"])
 '(custom-safe-themes
   '("bf387180109d222aee6bb089db48ed38403a1e330c9ec69fe1f52460a8936b66" "1fcf324026b6b238c2b8264c4f0a65145665f5650a0f1aae02004e24cab18db4" "17a0468b582214cc5d2cde23effba26221461032919bb39ef71789b1172e2ff5" "4bf168b6d8dbad61bfc578d5b8080c28278a35f5ff1b68211f5e4fe59521158e" "6600237c0a16104994ac8c6c77483aefc094ac50f09214097c161fdd1480e411" "a5cecd6f9f3ab6ac389398a89eab0d0f6ab0ac56b0e8549076296d314d009d98" "0785fd628133674f941317df29ea08b3a644622724431b67d9c1bafcffaaa51e" "bc7aa0706735d36bc0e693196ce6d1b5d5b8db1ae7a5b35ee40ddad373dd76fa" "6f4451966b64d1a1c08ba87faff6cf90a1fc8c28bba92dc0c376a56fa15ac639" "cdb5b070ae2df6ae67fd84e57847340a5a327b10b5c4f08ce9c68d5681240f42" "852996447e0372bd26ec748fc2d26dd43af3ef298c1513a9cfcc3cb39578ace5" "89c5075baa0ec67b12401f1693b0473d0a4db6eb61494c2596cadf7590f82e3d" "ba9f4b391fa1c1754f74e62e71ba50d169382214ff0b761a1abc684006b80763" "63ce681fa9ef9dde3c6c274ec2a225c1eea066107f69253e042a3c39079fd15b" "8038927c519d7fc6fee70aa15a8fb97a70269037ed787b8c17c4f84984fe3bde" "fdffb857c8f5e86f4c1c66b7232d3431f825d8cad9d809551653086ec444d71c" "9ae707c90810f41f1c969f8a2ce956eae095bfc7b24522f58f74db879f06db6c" "fb0a9fcec3435da44858c0f4954ebfb2f23bad99dba8a906cb9567867ca2e149" "fae5ccd01c1210214eafecca104759f468365978703621d7a838f972cf1be234" "60e00fe421246a024644cde32ef307cff70b55b14d11a0e3e3400ba372c84524" "ae096a2c7513ba01d53c33c9b76b4774009916af6cb694d4d903d5cf75f2ac8e" "ec44b49d4cf9bba316f68dfb03fb93d335704a700c870a0f93132c30df7b4d5a" "96005f97499f0549f921f81588f190f189b7acb8bbebbcbb9033cdd340118f80" "dc85ae30036b7190977cd527c13300b5c9134e2003e31990304f4bf71bb9c806" "f4627015994d684dd3b4d7462226a3160821fef3858d47d08982b00b0cbc99ec" "780489deb30f7c240cf49b8ddd7a48c3a6a963d0aab321d490ed64a2ca4733eb" "689bdf842051f32742432c94aba6c479dc83ca24a1b3f1df9a7e072d290094b8" "a7962c4ca4ba2de1cefd8ae621c5fdaf3710b0f59b877b613b5e3ed7b4ea84dd" "faa753a5b0a7c425eed5e3e62f4673105af028649f555343f84193a1f4fa66ae" "6f6f2e3e610fc7fb528728cf026b829344ba60b673899bc3808569df2d777eff" "5f5d19609b4477ea503f04dca830438c200884640326227833ebc00693e74bd6" "2cdc13ef8c76a22daa0f46370011f54e79bae00d5736340a5ddfe656a767fddf" "6979c58ebea17a1a5320e782a9f2208613b8ecad0ea4e6b9afa27b4feed33b46" default))
 '(fci-rule-color "#525252")
 '(jdee-db-active-breakpoint-face-colors (cons "#000000" "#80A0C2"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#000000" "#A2BF8A"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#000000" "#3f3f3f"))
 '(objed-cursor-color "#C16069")
 '(pdf-view-midnight-colors (cons "#eceff4" "#323334"))
 '(rustic-ansi-faces
   ["#323334" "#C16069" "#A2BF8A" "#ECCC87" "#80A0C2" "#B58DAE" "#86C0D1" "#eceff4"])
 '(vc-annotate-background "#323334")
 '(vc-annotate-color-map
   (list
    (cons 20 "#A2BF8A")
    (cons 40 "#bac389")
    (cons 60 "#d3c788")
    (cons 80 "#ECCC87")
    (cons 100 "#e3b57e")
    (cons 120 "#da9e75")
    (cons 140 "#D2876D")
    (cons 160 "#c88982")
    (cons 180 "#be8b98")
    (cons 200 "#B58DAE")
    (cons 220 "#b97e97")
    (cons 240 "#bd6f80")
    (cons 260 "#C16069")
    (cons 280 "#a0575e")
    (cons 300 "#804f54")
    (cons 320 "#5f4749")
    (cons 340 "#525252")
    (cons 360 "#525252")))
 '(vc-annotate-very-old-color nil)
 '(warning-suppress-log-types '((emacs))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
