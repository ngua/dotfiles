;;; doom-grayb-theme.el --- Theme -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Rory Tyler Hayford
;;
;; Author: Rory Tyler Hayford <rory.hayford@protonmail.com>
;; Maintainer: Rory Tyler Hayford <rory.hayford@protonmail.com>
;; Created: December 14, 2022
;; Modified: December 14, 2022
;; Version: 0.0.1
;;
;;; Commentary:
;;; _
;;; Code:


(require 'doom-themes)

;;
;;; Variables

(defgroup doom-grayb nil
  "Options for the `doom-grayb' theme."
  :group 'doom-themes)

(defcustom doom-grayb-brighter-modeline nil
  "If non-nil, more vivid colors will be used to style the mode-line."
  :group 'doom-grayb
  :type 'boolean)

(defcustom doom-grayb-padded-modeline doom-themes-padded-modeline
  "If non-nil, adds a 4px padding to the mode-line.
Can be an integer to determine the exact padding."
  :group 'doom-grayb
  :type '(choice integer boolean))

;;
;;; Theme definition

(def-doom-theme doom-grayb
  "A dark theme"

  ;; name        default   256           16
  ((bg         '("#1f1f1f" "black"       "black"  ))
   (fg         '("#ebebeb" "#bfbfbf"     "brightwhite"  ))

   ;; These are off-color variants of bg/fg, used primarily for `solaire-mode',
   ;; but can also be useful as a basis for subtle highlights (e.g. for hl-line
   ;; or region), especially when paired with the `doom-darken', `doom-lighten',
   ;; and `doom-blend' helper functions.
   (bg-alt     '("#262626" "black"       "black"        ))
   (fg-alt     '("#ebebeb" "#bfbfbf"     "white"        ))

   ;; These should represent a spectrum from bg to fg, where base0 is a starker
   ;; bg and base8 is a starker fg. For example, if bg is light grey and fg is
   ;; dark grey, base0 should be white and base8 should be black.
   (base0      '("#121212" "#121212" "black"      ))
   (base1      '("#202020" "#202020" "brightblack"))
   (base2      '("#373737" "#373737" "brightblack"))
   (base3      '("#414141" "#414141" "brightblack"))
   (base4      '("#727272" "#727272" "brightblack"))
   (base5      '("#919191" "#919191" "brightblack"))
   (base6      '("#a2a2a2" "#a2a2a2" "brightblack"))
   (base7      '("#c7c7c7" "#c7c7c7" "brightblack"))
   (base8      '("#dcdcdc" "#dcdcdc" "white"))

   (grey       base4                                 )
   (red        '("#bc8383" "#bc8383" "red"          ))
   (orange     '("#ffafaf" "#ffafaf" "brightred"    ))
   (green      '("#9ea29e" "#9ea29e" "green"        ))
   (teal       '("#ccdbe3" "#ccdbe3" "brightgreen"  ))
   (yellow     '("#eaeabd" "#eaeabd" "yellow"       ))
   (blue       '("#c8def5" "#c8def5" "brightblue"   ))
   (dark-blue  '("#9fb6cd" "#9fb6cd" "blue"         ))
   (magenta    '("#ffc0cb" "#ffc0cb" "brightmagenta"))
   (violet     '("#ffc0cb" "#ffc0cb" "magenta"      ))
   (cyan       '("#b4bdc8" "#b4bdc8" "brightcyan"   ))
   (dark-cyan  '("#c8def5" "#c8def5" "cyan"         ))

   ;; These are the "universal syntax classes" that doom-themes establishes.
   ;; These *must* be included in every doom themes, or your theme will throw an
   ;; error, as they are used in the base theme defined in doom-themes-base.
   (highlight      base5)
   (vertical-bar   (doom-darken base1 0.1))
   (selection      base6)
   (builtin        dark-blue)
   (comments       grey)
   (doc-comments   grey)
   (constants      base7)
   (functions      base8)
   (keywords       cyan)
   (methods        cyan)
   (operators      cyan)
   (type           base6)
   (strings        blue)
   (variables      base5)
   (numbers        orange)
   (region         `(,(doom-lighten (car bg-alt) 0.15) ,@(doom-lighten (cdr base1) 0.35)))
   (error          red)
   (warning        yellow)
   (success        green)
   (vc-modified    orange)
   (vc-added       green)
   (vc-deleted     red)

   ;; These are extra color variables used only in this theme; i.e. they aren't
   ;; mandatory for derived themes.
   (modeline-fg              fg)
   (modeline-fg-alt          base5)
   (modeline-bg              (if doom-grayb-brighter-modeline
                                 (doom-darken blue 0.45)
                               (doom-darken bg-alt 0.1)))
   (modeline-bg-alt          (if doom-grayb-brighter-modeline
                                 (doom-darken blue 0.475)
                               `(,(doom-darken (car bg-alt) 0.15) ,@(cdr bg))))
   (modeline-bg-inactive     `(,(car bg-alt) ,@(cdr base1)))
   (modeline-bg-inactive-alt `(,(doom-darken (car bg-alt) 0.1) ,@(cdr bg)))

   (-modeline-pad
    (when doom-grayb-padded-modeline
      (if (integerp doom-grayb-padded-modeline) doom-grayb-padded-modeline 4))))


  ;;;; Base theme face overrides
  (((line-number &override) :foreground base4)
   ((line-number-current-line &override) :foreground fg)
   (mode-line
    :background modeline-bg :foreground modeline-fg
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg)))
   (mode-line-inactive
    :background modeline-bg-inactive :foreground modeline-fg-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive)))
   (mode-line-emphasis :foreground (if doom-grayb-brighter-modeline base8 highlight))

   ;;;; css-mode <built-in> / scss-mode
   (css-proprietary-property :foreground orange)
   (css-property             :foreground green)
   (css-selector             :foreground blue)
   ;;;; doom-modeline
   (doom-modeline-bar :background (if doom-grayb-brighter-modeline modeline-bg highlight))
   (doom-modeline-buffer-file :inherit 'mode-line-buffer-id :weight 'bold)
   (doom-modeline-buffer-path :inherit 'mode-line-emphasis :weight 'bold)
   (doom-modeline-buffer-project-root :foreground green :weight 'bold)
   ;;;; flycheck
   (flycheck-error   :underline `(:style line :color ,red))
   (flycheck-warning :underline `(:style line :color ,orange))
   (flycheck-info :inherit 'normal)
   ;;;; font-lock
   (font-lock-constant-face :foreground base7 :weight 'medium)
   (font-lock-function-name-face :foreground base8 :weight 'semi-bold)
   (font-lock-keyword-face :foreground cyan :weight 'semi-bold)
   (font-lock-preprocessor-face :inherit 'font-lock-comment-face)
   (font-lock-regexp-grouping-backslash :inherit 'font-lock-builtin-face)
   (font-lock-regexp-grouping-construct :inherit 'font-lock-builtin-face)
   (font-lock-type-face :foreground base6 :weight 'medium)
   ;;;; haskell
   (haskell-interactive-face-result :inherit 'font-lock-comment-face)
   (haskell-interactive-face-garbage :inherit 'font-lock-comment-face)
   ;;;; ivy
   (ivy-current-match :background bg-alt :weight 'normal)
   (ivy-posframe :background "#181818")
   (ivy-modified-buffer :foreground red :weight 'bold)
   ;;;; LaTeX-mode
   (font-latex-math-face :foreground green)
   ;;;; markdown-mode
   (markdown-markup-face :foreground base5)
   (markdown-header-face :inherit 'bold :foreground blue)
   ((markdown-code-face &override) :background base2)
   ;;;; org
   ((org-block &override) :background base2 :extend t)
   ;;;; parens
   (paren-face-match    :foreground cyan :weight 'bold)
   (paren-face-mismatch :foreground red :weight 'bold)
   (paren-face-no-match :inherit 'paren-face-mismatch)

   ;;;; misc
   (fill-column-indicator :foreground base2)
   (highlight-numbers-number :foreground orange :weight 'light)
   (link :foreground base5 :weight 'bold)
   (helpful-heading :inherit 'variable-pitch))

  ;;;; Base theme variable overrides-
  ())

;;; doom-grayb.el ends here
