;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; (exec-path-from-shell-initialize)

(load-theme 'doom-grayb t)

(setq user-full-name "Rory Tyler Hayford"
      user-mail-address "rory.hayford@protonmail.com")

(setq doom-font (font-spec :family "Fira Code" :size 18 :weight 'light)
      doom-variable-pitch-font (font-spec :family "Fira Sans"))

(setq org-directory "~/org/")
(setq org-journal-dir "~/org/journal/")
(setq org-journal-date-format "%A, %d %B %Y")
(setq org-latex-pdf-process '("tectonic %f"))
(setq org-export-with-section-numbers nil)

(setq display-line-numbers-type 'relative)

(global-subword-mode 1)
(global-eldoc-mode -1)

(map! :n "L" #'evil-end-of-line
      :n "H" #'evil-first-non-blank
      :n "+" #'doom/increase-font-size
      :n "C-a" #'evil-numbers/inc-at-pt
      :n "C-x" #'evil-numbers/dec-at-pt
      :n "RET" #'+fold/toggle
      :n "\\" #'evil-ex)

(map! :leader
      :desc "Flycheck next error" "c n" #'flycheck-next-error
      :desc "Flycheck verify setup" "c v" #'flycheck-verify-setup
      :desc "Flycheck select checker" "c S" #'flycheck-select-checker
      :desc "Flycheck previous error" "c N" #'flycheck-previous-error
      :desc "Evil goto" "c g" #'evil-goto-definition
      :desc "Open URL at point" "o u" #'browse-url-at-point)

(map! :map (latex-mode-map text-mode-map markdown-mode-map org-mode-map)
      :n "j" #'evil-next-visual-line
      :n "k" #'evil-previous-visual-line
      :n "H" #'evil-beginning-of-visual-line
      :n "L" #'evil-end-of-visual-line)

(map! :map vterm-mode-map
      :i "M-p" #'vterm-send-up
      :i "M-n" #'vterm-send-down)

(defadvice! +org--suppress-mode-hooks-a (orig-fn &rest args)
  :around #'org-eldoc-get-mode-local-documentation-function
  (delay-mode-hooks (apply orig-fn args)))

;; Remove those awful line continuation chars in terminal mode
(set-display-table-slot standard-display-table 0 ?\ )

;; Put some padding on the top
(add-hook! 'buffer-list-update-hook (setq header-line-format " "))
(add-hook! 'prog-mode-hook #'display-fill-column-indicator-mode)

(defun +my/company-setup ()
  (setq company-tooltip-maximum-width 70)
  (setq company-selection-wrap-around t)
  (setq company-idle-delay 0.8))

(add-hook! 'company-mode-hook #'+my/company-setup)

(setq company-backends
      '(company-capf
        company-keywords
        company-dabbrev-code
        company-yasnippet
        company-files
        company-dabbrev))

(after! lsp-mode
  (setq lsp-progress-function #'ignore) ; WHY? The most irritating "feature" ever
  (setq lsp-enable-snippet nil)
  (setq lsp-ui-sideline-delay 1.5)
  (setq lsp-enable-symbol-highlighting nil)
  (setq lsp-signature-render-documentation nil)
  (setq lsp-eldoc-enable-hover nil)
  ;; (setq lsp-enable-folding nil)
  (setq lsp-enable-file-watchers nil)
  (setq lsp-enable-server-download nil)
  (setq lsp-lens-enable nil)
  (setq lsp-enable-suggest-server-download nil))

(add-hook! 'lsp-ui-mode-hook
  (setq lsp-ui-sideline-actions-icon nil)
  (advice-add #'lsp-ui-sideline--code-actions :around #'doom-shut-up-a))

;; Haskell settings
(defun +my/haskell-setup ()
  (setq haskell-mode-stylish-haskell-path "fourmolu")
  (setq lsp-haskell-formatting-provider "fourmolu")
  (setq flycheck-haskell-hlint-executable "hlint")
  (setq haskell-process-type 'cabal-new-repl)
  (add-hook 'lsp-after-initialize-hook
            (lambda () (flycheck-add-next-checker 'lsp 'haskell-hlint)))
  (set-formatter! 'cabal-fmt "cabal-fmt" :modes '(haskell-cabal-mode))
  (setq flycheck-hlintrc ".hlint.yaml")
  ;; Lsp's hlint doesn't respect config file
  (setq lsp-haskell-hlint-on nil)
  (haskell-indent-mode 0)
  (setq-local tab-width 2)
  (setq haskell-interactive-prompt " λ  ")
  (setq haskell-interactive-prompt-cont " λ|  ")
  (setq-local eldoc-documentation-function #'ignore)
  (delete "proc" haskell-font-lock-keywords))

(setq-default flycheck-disabled-checkers '(haskell-ghc haskell-stack-ghc))

(add-hook! '(haskell-mode-hook haskell-literate-mode-hook) #'+my/haskell-setup)
(add-hook! 'haskell-literate-mode-hook #'lsp)
(remove-hook! 'haskell-interactive-mode-hook 'highlight-numbers-mode)

(defun +my/hs-work-shell ()
  (setq haskell-process-args-cabal-repl '())
  (setq haskell-process-wrapper-function 'identity)
  (setq lsp-haskell-server-wrapper-function 'identity)
  (setq lsp-haskell-formatting-provider "fourmolu")
  (setq lsp-haskell-server-path "haskell-language-server"))

;; Rust settings
(after! rustic
  (setq rustic-lsp-server 'rust-analyzer))

;; Repl stuff
(defun +my/repl-setup ()
  (setq comint-scroll-show-maximum-output nil)
  (setq-local scroll-margin 0)
  (setq haskell-process-show-debug-tips nil)
  (setq haskell-interactive-popup-errors nil)
  (setq-local recenter-positions '(top middle bottom)))

(add-hook! 'comint-mode-hook #'+my/repl-setup)
(add-hook! 'haskell-interactive-mode-hook #'+my/repl-setup)

;; Latex
(defun +my/latex-setup ()
  (setq lsp-latex-build-executable "tectonic")
  (setq lsp-latex-build-args
        '("%f" "--synctex" "--keep-logs" "--keep-intermediates")))

(add-hook! '(tex-mode-hook latex-mode-hook) #'+my/latex-setup)

(defun +my/beamer-bold (contents backend _info)
  (when (eq backend 'beamer)
    (replace-regexp-in-string "\\`\\\\[A-Za-z0-9]+" "\\\\textbf" contents)))

;; Dhall & nix
(add-hook 'dhall-mode-local-vars-hook #'lsp!)
(add-hook 'nix-mode-local-vars-hook #'lsp!)

(set-window-margins (selected-window) 1 1)
(setq scroll-margin 4)

(use-package! dired
  :hook (dired-mode . dired-hide-details-mode))

(use-package! engrave-faces-latex :after ox-latex :init
              (setq org-latex-listings 'engraved))

(use-package! anki-editor :after org :init (setq anki-editor-use-math-jax t))

(defun +my/current-file ()
  (file-name-nondirectory (buffer-file-name)))

;; IRC
(after! circe
  (set-irc-server! "irc.libera.chat"
    `(:tls t
      :port 6697
      :nick "ngua"
      :sasl-username ,(+pass-get-user "irc/libera")
      :sasl-password (lambda (&rest _) (+pass-get-secret "irc/libera"))
      :channels ("#nixos" "#emacs" "#haskell"))))

(advice-add #'circe--irc-display-event :around #'doom-shut-up-a)

;; Make fira code ligatures work in popups and other non-prog-mode buffers
(when (window-system)
  (set-frame-font "Fira Code"))
(let ((alist '((33 . ".\\(?:\\(?:==\\|!!\\)\\|[!=]\\)")
               (35 . ".\\(?:###\\|##\\|_(\\|[#(?[_{]\\)")
               (36 . ".\\(?:>\\)")
               (37 . ".\\(?:\\(?:%%\\)\\|%\\)")
               (38 . ".\\(?:\\(?:&&\\)\\|&\\)")
               (42 . ".\\(?:\\(?:\\*\\*/\\)\\|\\(?:\\*[*/]\\)\\|[*/>]\\)")
               (43 . ".\\(?:\\(?:\\+\\+\\)\\|[+>]\\)")
               (45 . ".\\(?:\\(?:-[>-]\\|<<\\|>>\\)\\|[<>}~-]\\)")
               (46 . ".\\(?:\\(?:\\.[.<]\\)\\|[.=-]\\)")
               (47 . ".\\(?:\\(?:\\*\\*\\|//\\|==\\)\\|[*/=>]\\)")
               (48 . ".\\(?:x[a-zA-Z]\\)")
               (58 . ".\\(?:::\\|[:=]\\)")
               (59 . ".\\(?:;;\\|;\\)")
               (60 . ".\\(?:\\(?:!--\\)\\|\\(?:~~\\|->\\|\\$>\\|\\*>\\|\\+>\\|--\\|<[<=-]\\|=[<=>]\\||>\\)\\|[*$+~/<=>|-]\\)")
               (61 . ".\\(?:\\(?:/=\\|:=\\|<<\\|=[=>]\\|>>\\)\\|[<=>~]\\)")
               (62 . ".\\(?:\\(?:=>\\|>[=>-]\\)\\|[=>-]\\)")
               (63 . ".\\(?:\\(\\?\\?\\)\\|[:=?]\\)")
               (91 . ".\\(?:]\\)")
               (92 . ".\\(?:\\(?:\\\\\\\\\\)\\|\\\\\\)")
               (94 . ".\\(?:=\\)")
               (119 . ".\\(?:ww\\)")
               (123 . ".\\(?:-\\)")
               ;; (124 . ".\\(?:\\(?:|[=|]\\)\\|[=>|]\\)")
               (126 . ".\\(?:~>\\|~~\\|[>=@~-]\\)"))))
  (dolist (char-regexp alist)
    (set-char-table-range composition-function-table (car char-regexp)
                          `([,(cdr char-regexp) 0 font-shape-gstring]))))

(setq flycheck-display-errors-delay 15)
(setq flycheck-check-syntax-automatically '(save))
(setq auto-save-no-message t)

(setq fancy-splash-image "~/.config/doom/lambda.svg")

(defun +my/ivy-posframe-get-size ()
  (let ((height (or ivy-posframe-height (or ivy-height 10)))
        (width (min (or ivy-posframe-width 200) (round (* .75 (frame-width))))))
    (list :height height :width width :min-height height :min-width width)))

(setq ivy-posframe-size-function '+my/ivy-posframe-get-size)

(setq doom-themes-treemacs-theme "doom-colors")

(setq +lookup-provider-url-alist
      '(("Doom Emacs issues" "https://github.com/hlissner/doom-emacs/issues?q=is%%3Aissue+%s")
        ("DuckDuckGo"        +lookup--online-backend-duckduckgo "https://duckduckgo.com/?q=%s")
        ("Github"            "https://github.com/search?ref=simplesearch&q=%s")
        ("Wikipedia"         "https://wikipedia.org/search-redirect.php?language=en&go=Go&search=%s")
        ("MDN"               "https://developer.mozilla.org/en-US/search?q=%s")
        ("NixOS Wiki"        "https://nixos.wiki/index.php?search=%s")
        ("Arch Wiki"         "https://wiki.archlinux.org/index.php?search=%s")
        ("GHC User's Guide"  "https://ghc.gitlab.haskell.org/ghc/doc/users_guide/search.html?q=%s")
        ("Hoogle"            "https://hoogle.haskell.org/?hoogle=%s")
        ("Rust Docs"         "https://doc.rust-lang.org/std/?search=%s")))

(add-hook! '(haskell-cabal-mode-hook yaml-mode-hook) #'display-line-numbers-mode)

;; FIXME tree-sitter
;; (setq +tree-sitter-hl-enabled-modes t)
